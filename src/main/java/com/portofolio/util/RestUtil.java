package com.portofolio.util;

import java.util.List;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;


/**
 * Rest Util Class
 * 
 * @author Adik
 */
public class RestUtil {
	
	public static final String MESSAGE = "message";

	public static final String STATUS_CODE = "statusCode";

	public static final String STATUS = "status";

	public static final String RESULT = "result";

	private static final String CONTENT_TYPE = "Content-Type";

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <T> ResponseEntity<T> getJsonResponse(T src) {
		Map<String,Object> srcMap=(Map<String, Object>) src;
		HttpStatus status=(HttpStatus) srcMap.get(RESULT);
		if(CommonUtil.isNullOrEmpty(status)) {
			status= HttpStatus.OK;
		}
		HttpHeaders headers = new HttpHeaders();
//		if (null != srcMap) {
//			for (String key : srcMap.keySet()) {
//				if(CommonUtil.isNotNullOrEmpty(srcMap.get(key))) {
//					if(key.equalsIgnoreCase("message") ||  
//							key.equalsIgnoreCase("statusCode") || 
//							key.equalsIgnoreCase("status") ||
//							key.equalsIgnoreCase("result")) {
//						headers.add(key, srcMap.get(key).toString());
//					}
//				}
//			}
//		}

		// headers.add(ACCESS_CONTROL_ALLOW_ORIGIN, "*");
		headers.set(CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		return new ResponseEntity(srcMap, headers, status);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <T> ResponseEntity<T> getJsonResponse(T src, HttpStatus status,
			Map<String, String> mapHeaderMessage) {

		HttpHeaders headers = new HttpHeaders();

		if (null != mapHeaderMessage) {
			for (String key : mapHeaderMessage.keySet()) {
				headers.add(key, mapHeaderMessage.get(key));
			}
		}

		Map<String, Object> map = CommonUtil.createMap();
		if (null != mapHeaderMessage) {
			for (String key : mapHeaderMessage.keySet()) {
				map.put(key, mapHeaderMessage.get(key));
			}
		}
		map.put("data", src);
		// headers.add(ACCESS_CONTROL_ALLOW_ORIGIN, "*");
		headers.set(CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		return new ResponseEntity(map, headers, status);
	}
	
	public static <T> ResponseEntity<T> getJsonHttptatus(HttpStatus status) {

		return new ResponseEntity<T>(status);

	}


}